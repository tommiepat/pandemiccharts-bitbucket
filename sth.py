from flask import Flask, render_template, request, Response
import plotly
import plotly.graph_objs as go
#import pandas as pd
import numpy as np
import json
import requests
import csv
import urllib.request
import codecs
#from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
#from matplotlib.figure import Figure
#import matplotlib.pyplot as plt
import io
#import base64

app = Flask(__name__)

@app.route('/')
def index():
    feature = 'Italy'
    lines = create_plot(feature)
    tpmlines = create_plot('tpmItaly')
    # http://benalexkeen.com/creating-graphs-using-flask-and-d3/
    #jsonfile = open('file.json', 'w')
    #with open('per-nac.csv', newline='') as csvfile: 
    #    reader = csv.DictReader(csvfile)
    #    for row in reader:
    #        json.dump(row, jsonfile)
    #        jsonfile.write('\n')
    ##chch = json.dumps(reader, indent=2)
    ##prim = np.genfromtxt('per-nac.csv', delimiter=',')
    ##prim = prim.tolist()
    ## chart_data = json.dumps(chch, indent=2)
    #data = {'chart_data': open('file.json', 'r').read()}

    # The following lines probably worked.
    #df = pd.read_csv('per-nac.csv')
    ##print(df)
    #print(type(df))
    #chda = df.to_dict(orient='records')
    #chjson = json.dumps(chda, indent=2)
    #data = {'chart_data': chjson}

    #jjiter=jj.iter_lines()
    #jjarr=np.genfromtxt(jj, delimiter=',')
    #print(type(jjiter))
    #reader=csv.reader(jjiter, delimiter=',')
    #print(bytes.decode(reader))
    #with open(jj, newline='') as csvfile:
    #    spamreader = csv.reader(csvfile, delimiter=',')
    #    for row in spamreader:
    #        print(', '.join(row))
    ##response=urllib.request.urlopen(itaurl)
    ##row_count=sum(1 for row in response)
    #print(row_count)
    #response.seek(0) returns io.UnsupportedOperation
    #print(response)
    #cr=csv.reader(response)
    #print(cr)
    #for row in cr:
    #    print (row)
    #encoding = 'utf-8'
    #reader.decode(encoding)

    return render_template('index.html', plot=lines, tpmplot=tpmlines) 

def create_plot(feature):
    gen = np.genfromtxt('per-nac.csv', delimiter=',')
    N = len(gen[:, 11])-1
    y1 = gen[:, 10][10:N+1]
    y2 = gen[:, 11][10:N+1]
    #print(type(y2)) returns 'numpy.ndarray'

    if feature == 'Italy':
        y = np.multiply(np.divide(y1, y2), 100)
        x = np.linspace(-13, N-23, N-9)
        itaurl = 'https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv'
        response = urllib.request.urlopen(itaurl)
        #row_count=sum(1 for row in response)
        # https://stackoverflow.com/questions/3428532/how-to-import-a-csv-file-using-python-with-headers-intact-where-first-column-is
        # The first answer might be useful to delete the first row. Maybe use pop?
        csvit = csv.reader(codecs.iterdecode(response, 'utf-8'))
        csvl = list(csvit)
        # del csvl[0]
        #csvfile.seek(0) returns error, object has no attribute
        y7 = []
        y4 = []
        for i in range(1, len(csvl)) :
            y7.append(csvl[i][11])
            y4.append(csvl[i][12])
        # del y7[0]
        # del y4[0]
        y7 = list(map(int, y7))
        y3 = np.array(y7)
        #y3=np.genfromtxt('ita-naz.csv', delimiter=',')[:,10][1:N+1]
        #y4=np.genfromtxt('ita-naz.csv', delimiter=',')[:,11][1:N+1]
        y4 = list(map(int, y4))
        y4 = np.array(y4)

        yit = np.multiply(np.divide(y3, y4), 100)  
        xit = np.linspace(1, len(yit), len(yit))

        # Create a trace
        data = [go.Scatter(
            x = x,
            y = y,
            mode = 'lines+markers',
            name = 'Peru'
        ), go.Scatter(
            x = xit,
            y = yit,
            mode = 'lines+markers',
            name='Italy'
        )]

    elif feature == 'Florida':
        y=np.multiply(np.divide(y1, y2), 100)  
        x = np.linspace(-13, N-23, N-9)
        flreq=requests.get(url='https://covidtracking.com/api/states/daily?state=FL').json()
        flpos=[]
        fltot=[]
        for i in range(len(flreq)) :
            flpos.insert(0, flreq[i]['positive'])
            fltot.insert(0, flreq[i]['totalTestResults'])
        flpos=np.array(flpos)
        fltot=np.array(fltot)
        yfl=np.multiply(np.divide(flpos, fltot), 100)
        xfl=np.linspace(-11, len(flreq)-12, len(flreq))

        # Create a trace
        data = [go.Scatter(
            x = x,
            y = y,
            mode = 'lines+markers',
            name='Peru'
        ), go.Scatter(
            x = xfl,
            y = yfl,
            mode = 'lines+markers',
            name='Florida'
        )]


    elif feature == 'Peru':
        y=np.multiply(np.divide(y1, y2), 100)  
        x = np.linspace(-13, N-23, N-9)

        # Create a trace
        data = [go.Scatter(
            x = x,
            y = y,
            mode = 'lines+markers'
        )]

    elif feature == 'Penn':
        # Todo: work on algorithm that detects null for pending. And algorithm that
        # detects the day zero, namely the day the region reaches more than 100 cases for
        # the first time.  
        # Also, measure compiling time.    
        y=np.multiply(np.divide(y1, y2), 100)  
        x = np.linspace(-13, N-23, N-9)
        flreq=requests.get(url='https://covidtracking.com/api/states/daily?state=PA').json()
        flpos=[]
        fltot=[]
        for i in range(len(flreq)) :
            flpos.insert(0, flreq[i]['positive'])
            fltot.insert(0, flreq[i]['totalTestResults'])
        flpos=np.array(flpos)
        fltot=np.array(fltot)
        yfl=np.multiply(np.divide(flpos, fltot), 100)
        xfl=np.linspace(-12, len(flreq)-13, len(flreq)) # First find the position of 
        # day zero on the json file. Then substract that from the total number of
        # elements minus 1. FOr example, in this case the position of the day zero was 12.
        # Then 24 minus 12 is 12. Then negative 13 is the first argument in this line.  

        # Create a trace
        data = [go.Scatter(
            x = x,
            y = y,
            mode = 'lines+markers',
            name='Peru'
        ), go.Scatter(
            x = xfl,
            y = yfl,
            mode = 'lines+markers',
            name='Pennsylvania'
        )]

    elif feature == 'tpmPeru':
        y=np.divide(y2, 33)  
        x = np.linspace(-13, N-23, N-9)

        # Create a trace
        data = [go.Scatter(
            x = x,
            y = y,
            mode = 'lines+markers'
        )]

    elif feature == 'tpmFlorida':
        y=np.divide(y2, 33)  
        x = np.linspace(-13, N-23, N-9)
        flreq=requests.get(url='https://covidtracking.com/api/states/daily?state=FL').json()
        #flpos=[]
        flTotalTest=[]
        for i in range(len(flreq)) :
            #flpos.insert(0, flreq[i]['positive'])
            flTotalTest.insert(0, flreq[i]['totalTestResults'])
        # flpos=np.array(flpos)
        flTotalTest=np.array(flTotalTest)

        flCensus =requests.get(url='https://api.census.gov/data/2019/pep/population?get=POP,NAME&for=state:12').json()
        # Use the following if you want to see the population of another states:
        # https://api.census.gov/data/2019/pep/population?get=POP,NAME&for=state:*
        flPOP=flCensus[1][0]
        yfl=np.divide(flTotalTest, int(flPOP)/1000000.0)
        xfl=np.linspace(-11, len(flreq)-12, len(flreq))

        # Create a trace
        data = [go.Scatter(
            x = x,
            y = y,
            mode = 'lines+markers',
            name='Peru'
        ), go.Scatter(
            x = xfl,
            y = yfl,
            mode = 'lines+markers',
            name='Florida'
        )]

    else:
        y=np.divide(y2, 33)  
        x = np.linspace(-13, N-23, N-9)
        itaurl='https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv'
        response=urllib.request.urlopen(itaurl)
        csvit=csv.reader(codecs.iterdecode(response, 'utf-8'))
        csvl=list(csvit)
        y4=[]
        for line in csvl :
            y4.append(line[12])
        del y4[0]
        y4=list(map(int, y4))
        y4=np.array(y4)

        yit=np.divide(y4, 60.5)  
        xit = np.linspace(1, len(yit), len(yit))

        # Create a trace
        data = [go.Scatter(
            x = x,
            y = y,
            mode = 'lines+markers',
            name='Peru'
        ), go.Scatter(
            x = xit,
            y = yit,
            mode = 'lines+markers',
            name='Italy'
        )]



    graphJSON = json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)    
    #print(graphJSON)
    return graphJSON


#def create_mpl(mplfeature):
#
#    itaurl='https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv'
#    gen = np.genfromtxt('per-nac.csv', delimiter=',')
#    N = len(gen[:, 11])-1
#    y1 = gen[:, 10][10:N+1]
#    y2 = gen[:, 11][10:N+1]
#
#    if mplfeature == 'tpmmplIta':
#        fig = Figure()
#        axis = fig.add_subplot(1, 1, 1)
#        xper = np.linspace(-13, N-23, N-9)
#        yper = np.divide(y2, 33)
#        response = urllib.request.urlopen(itaurl)
#        csvl = list(csv.reader(codecs.iterdecode(response, 'utf-8')))
#        yit = []
#        for i in range(1, len(csvl)):
#            yit.append(csvl[i][12])
#        yit = list(map(int, yit))
#        yit = np.array(yit)
#        yit = np.divide(yit, 60.5)
#        xit = np.linspace(1, len(yit), len(yit))
#        axis.plot(xper, yper)
#        axis.plot(xit, yit)
#        return fig  
#
#    elif mplfeature == 'tpmmplPer':
#        fig = Figure()
#        axis = fig.add_subplot(1, 1, 1)
#        xper = np.linspace(-13, N-23, N-9)
#        yper = np.divide(y2, 33)
#        axis.plot(xper, yper)
#        return fig  



@app.route('/bar', methods=['GET', 'POST'])
def change_features():

    feature = request.args['selected']
    graphJSON= create_plot(feature)

    return graphJSON



@app.route('/tpm', methods=['GET', 'POST'])
def change_features1():

    feature = request.args['selected']
    graphJSON= create_plot(feature)

    return graphJSON

#@app.route('/mpl', methods=['GET', 'POST'])
#def plot_png():
#    feature = request.args['selected']
#    fig = create_mpl(feature)
#    output = io.BytesIO()
#    FigureCanvas(fig).print_png(output)
#    return Response(output.getvalue(), mimetype='image/png')

#def change_features2():

#    feature = request.args['selected']
#    #graphJSON= create_plot(feature)
#    graphmpl = create_mpl(feature)
#
#    # return graphJSON
#    return graphmpl

#@app.route('/plot')
#def build_plot():
#
#    img = io.BytesIO()
#    import pdb; pdb.set_trace()
#    print(img)
#
#    y = [1,2,3,4,5]
#    x = [0,2,1,3,4]
#    plt.plot(x,y)
#    plt.savefig(img, format='png')
#    print(img)
#    img.seek(0)
#    import pdb; pdb.set_trace()
#    print(img.seek(0))
#    print(img)
#    plot_url = base64.b64encode(img.getvalue()).decode()
#
#    return '<img src="data:image/png;base64,{}">'.format(plot_url)



if __name__ == '__main__':
    app.run()
