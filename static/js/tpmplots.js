$('#second_cat').on('change',function(){

    $.ajax({
        url: "/tpm",
        type: "GET",
        contentType: 'application/json;charset=UTF-8',
        data: {
            'selected': document.getElementById('second_cat').value

        },
        dataType:"json",
        success: function (data) {
            Plotly.newPlot('tpmgraph', data, {dragmode: 'pan'}, {} );
        }
    });
})
